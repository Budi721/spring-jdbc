package repositories;

import model.Product;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public record ProductRepository(JdbcTemplate jdbcTemplate) {

    public void addProduct(Product product) {
        var sql = "INSERT INTO product VALUES (NULL, ?, ?)";

        jdbcTemplate.update(sql, product.getName(), product.getPrice());
    }

    public List<Product> getProducts() {
        var sql = "SELECT * FROM product";
        return jdbcTemplate.query(sql, (rs, rowNum) -> {
            var result = new Product();
            result.setId(rs.getInt("id"));
            result.setName(rs.getString("name"));
            result.setPrice(rs.getDouble("price"));
            return result;
        });
    }
}
